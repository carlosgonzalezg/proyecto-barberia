/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author estudiante
 */
public interface CRUD {
    public boolean crear(Object param);

    public boolean eliminar(String primaryKey);
    
    public List<Object> mostrarContenido();

   public List<Object>  consultar(String param);
   
   public boolean  actualizar(String pk,String param);
}
