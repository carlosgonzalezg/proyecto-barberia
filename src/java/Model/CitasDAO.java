package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CitasDAO implements CRUD {

    MysqlDbAdapter ms = new MysqlDbAdapter();

    @Override
    public boolean crear(Object param) {
        Connection connection = ms.getConnection();
        try {
            CitasDTO cita = (CitasDTO) param;
            PreparedStatement statement = connection.prepareStatement("INSERT INTO citas(fecha, cedulaCliente,tipoServicio) VALUES (" + "STR_TO_DATE(" + " '" + cita.getFecha() + "', '%d-%m-%Y %H:%i:%s' )" + ",?,?)");
            
            statement.setLong(1, cita.getCedulaCliente());
            statement.setLong(2, cita.getIdServicio());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean eliminar(String primaryKey) {
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM citas WHERE id =?");
            statement.setInt(1, Integer.parseInt(primaryKey));
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public List<Object> mostrarContenido() {
        Connection connection = ms.getConnection();
        ArrayList<Object> citas = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM citas");
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                CitasDTO cita = new CitasDTO(results.getInt(3), results.getString(2), results.getInt(4));
                cita.setId(results.getInt(1));
                citas.add(cita);
            }
            return citas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public List<Object> consultar(String idServicio) {
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM citas WHERE Nombre  LIKE '%"+idServicio+"%'");
            
            List<Object> citas = new ArrayList<>();
            
            ResultSet results = statement.executeQuery();
            while(results.next()){
                CitasDTO cita = new CitasDTO(results.getInt(3), results.getDate(2).toString(), results.getInt(4));
                cita.setId(results.getInt(1));
                citas.add(cita);
            }
                return citas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public boolean actualizar(String pk,String param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
