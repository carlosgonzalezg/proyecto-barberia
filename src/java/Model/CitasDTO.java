
package Model;

import java.util.Date;

public class CitasDTO {
    
    private int cedulaCliente;
    private String fecha;
    private int id;
    private int idServicio;

    public CitasDTO(int cedulaCliente, String fecha, int idServicio) {
        this.cedulaCliente = cedulaCliente;
        this.fecha = fecha;
        this.idServicio = idServicio;
    }

    public int getCedulaCliente() {
        return cedulaCliente;
    }

    public void setCedulaCliente(int cedulaCliente) {
        this.cedulaCliente = cedulaCliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }
}
