/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ClienteDAO implements CRUD{

    MysqlDbAdapter ms= new MysqlDbAdapter();

    @Override
    public boolean crear(Object param) {
        Connection connection = ms.getConnection();
        try {
            ClienteDTO cliente = (ClienteDTO)param;
            PreparedStatement statement = connection.prepareStatement("INSERT INTO cliente(nombresYApellidos, cedula, fechaDeNacimiento,email,telefono,password) VALUES (?,?," + "STR_TO_DATE(" + "'" + cliente.getFechaDeNacimiento() + "', '%d-%m-%Y' )" + ",?,?,?)");
            
            statement.setString(1, cliente.getNombre());
            statement.setLong(2, cliente.getCedula());
            statement.setString(3, cliente.getEmail());
            statement.setLong(4, cliente.getTelefono());
            statement.setString(5, cliente.getPassword());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public boolean eliminar(String primaryKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Object> mostrarContenido() {
           Connection connection = ms.getConnection();
        ArrayList<Object> listaDeClientes = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM cliente");
            ResultSet results = statement.executeQuery();
            while(results.next()){
                listaDeClientes.add(new ClienteDTO(results.getString(1),results.getInt(2),results.getString(3),results.getString(4),results.getLong(5),results.getString(6)));
            }
                return listaDeClientes;
            } 
        catch (Exception e) {
                    e.printStackTrace();                    
                return null;
        }
        finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public List<Object> consultar(String param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(String pk, String param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
