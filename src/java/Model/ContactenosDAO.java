/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ContactenosDAO implements CRUD{

    public ContactenosDAO() {
    }

     MysqlDbAdapter ms = new MysqlDbAdapter();
    @Override
    public boolean crear(Object param) {
        Connection connection = ms.getConnection();
        try {
            ContactenosDTO contacto=(ContactenosDTO)param;
            PreparedStatement statement = connection.prepareStatement("INSERT INTO contactenos(nombre, correo,asunto,mensaje) VALUES (?,?,?,?)");
            statement.setString(1, contacto.getNombre());
            statement.setString(2, contacto.getCorreo());
            statement.setString(3, contacto.getAsunto());
            statement.setString(4, contacto.getMensaje());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public boolean eliminar(String primaryKey) {
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM contactenos WHERE id =?");
            statement.setInt(1, Integer.parseInt(primaryKey));
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public List<Object> mostrarContenido() {
         Connection connection = ms.getConnection();
        ArrayList<Object> lista = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM contactenos");
            ResultSet results = statement.executeQuery();
            while(results.next()){
                lista.add(new ContactenosDTO(results.getInt(1),results.getString(2), results.getString(3), results.getString(4), results.getString(5)));
            }
                return lista;
            } 
        catch (Exception e) {
                    e.printStackTrace();                    
                return null;
        }
        finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public List<Object> consultar(String param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(String pk, String param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    



}
