/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ImagenDAO {

    public ImagenDAO() {
    }
    
    MysqlDbAdapter ms = new MysqlDbAdapter();
     public List<ImagenDTO> mostrarContenido() {
         Connection connection = ms.getConnection();
        ArrayList<ImagenDTO> listaDeServicios = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM imagenes");
            ResultSet results = statement.executeQuery();
            while(results.next()){
                listaDeServicios.add(new ImagenDTO(results.getLong(1),results.getString(2)));
            }
                return listaDeServicios;
            } 
        catch (Exception e) {
                    e.printStackTrace();                    
                return null;
        }
        finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }
     public boolean guardarImagen(ImagenDTO imagen){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO imagenes(url) VALUES (?)");
            statement.setString(1, imagen.getUrl());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }
      public boolean deleteImagen(long id){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM imagenes WHERE id=?");
            statement.setLong(1, id);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }     
    
}
