/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author carlos
 */
public class ImagenDTO {
    private String url;
    private long id;

    
    public ImagenDTO(long id,String url) {
        this.id=id;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
}
