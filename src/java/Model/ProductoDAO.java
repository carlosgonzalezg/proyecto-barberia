/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author CARLOS
 */
public class ProductoDAO {

    public ProductoDAO() {
    }
    MysqlDbAdapter ms= new MysqlDbAdapter();
    public List<ProductoDTO> findAllProducts(){
        Connection connection = ms.getConnection();
        ArrayList<ProductoDTO> productList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT id , Nombre, Precio, urlImagen FROM products");
            ResultSet results = statement.executeQuery();
            while(results.next()){
                productList.add(new ProductoDTO(results.getLong(1),results.getString(2), (int)results.getDouble(3),results.getString(4)));
            
            }
                return productList;
            } 
        catch (Exception e) {
                    e.printStackTrace();                    
                return null;
        }
        finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }
     public boolean saveProduct(ProductoDTO product){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Products(Nombre, Precio,urlImagen) VALUES (?,?,?)");
            statement.setString(1, product.getNombreProducto());
            statement.setDouble(2, product.getPrecioProducto());
            statement.setString(3, product.getUrlImagen());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }
      public boolean deleteProduct(Long idProduct){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Products WHERE Id =?");
            statement.setLong(1, idProduct);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }    
      
    public boolean updateProduct(Long idProduct, double nuevoPrecio){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("Update products Set Precio='"+nuevoPrecio+"' Where Id=?");
            statement.setDouble(1, idProduct);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }  
    
    public List<ProductoDTO> buscarProducto(String nombre){
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Products WHERE Nombre  LIKE '%"+nombre+"%'");
            
            List<ProductoDTO> productos = new ArrayList<ProductoDTO>();
            
            ResultSet results = statement.executeQuery();
            while(results.next()){
                productos.add(new ProductoDTO(results.getLong(1),results.getString(2), (int)results.getDouble(3),results.getString(4)));
            }
                return productos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }
}
