/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DecimalFormat;

/**
 *
 * @author CARLOS
 */
public class ProductoDTO{
    private long idProducto;
    private String nombreProducto;
    private int precioProducto;
    private String urlImagen;
    

    public ProductoDTO() {
    }

    
    public ProductoDTO(long idProducto,String nombreProducto, int precioProducto, String urlImagen) {
        this.idProducto=idProducto;
        this.nombreProducto = nombreProducto;
        this.precioProducto = precioProducto;
        this.urlImagen = urlImagen;
    }
    public ProductoDTO(String nombreProducto, int precioProducto, String urlImagen) {
        this.nombreProducto = nombreProducto;
        this.precioProducto = precioProducto;
        this.urlImagen = urlImagen;
    }
    
    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }


    public long getIdProducto() {
        return idProducto;
    }

    @Override
    public String toString() {
        return "ProductoDTO{" + "idProducto=" + idProducto + ", nombreProducto=" + nombreProducto + ", precioProducto=" + precioProducto + '}';
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public void setPrecioProducto(int precioProducto) {
        this.precioProducto = precioProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public int getPrecioProducto() {
        return precioProducto;
    }
    public String getPrecioFormato(){
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        return formatea.format(this.getPrecioProducto());
    }
    
}
