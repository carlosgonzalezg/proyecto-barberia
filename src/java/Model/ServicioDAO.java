
package Model;

import DATABASE.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class ServicioDAO implements CRUD{

     MysqlDbAdapter ms = new MysqlDbAdapter();
    @Override
    public boolean crear(Object param) {
        Connection connection = ms.getConnection();
        try {
            ServicioDTO servicio=(ServicioDTO)param;
            PreparedStatement statement = connection.prepareStatement("INSERT INTO servicio(id, nombre, precio,descripcion) VALUES (?,?,?,?)");
            statement.setInt(1, servicio.getID());
            statement.setString(2, servicio.getNombre());
            statement.setInt(3, servicio.getPrecio());
            statement.setString(4, servicio.getDescripcion());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public boolean eliminar(String primaryKey) {
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM servicio WHERE id =?");
            statement.setInt(1, Integer.parseInt(primaryKey));
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public List<Object> mostrarContenido() {
         Connection connection = ms.getConnection();
        ArrayList<Object> listaDeServicios = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM servicio");
            ResultSet results = statement.executeQuery();
            while(results.next()){
                listaDeServicios.add(new ServicioDTO(results.getInt(1),results.getString(2), results.getInt(3), results.getString(4)));
            }
                return listaDeServicios;
            } 
        catch (Exception e) {
                    e.printStackTrace();                    
                return null;
        }
        finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public List<Object> consultar(String param) {
        Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM servicio WHERE nombre  LIKE '%"+param+"%'");
            
            List<Object> servicios = new ArrayList<>();
            
            ResultSet results = statement.executeQuery();
            while(results.next()){
                servicios.add(new ServicioDTO(results.getInt(1),results.getString(2), results.getInt(3),results.getString(4)));
            }
                return servicios;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public boolean actualizar(String pk,String param) {
         Connection connection = ms.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("Update servicio Set precio='"+param+"' Where id=?");
            statement.setDouble(1, Integer.parseInt(pk));
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            try {
                connection.close();
            } catch (Exception e) {}
        }
    }

}
