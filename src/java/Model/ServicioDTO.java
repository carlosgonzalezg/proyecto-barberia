
package Model;

import java.text.DecimalFormat;
import java.util.*;


public class ServicioDTO {
    private int ID;
    private String nombre;
    private int precio;
    private String descripcion;

    public ServicioDTO(int ID, String nombre, int precio, String descripcion) {
        this.ID = ID;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    } 
    
    public String getPrecioFormato(){
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        return formatea.format(this.getPrecio());
    }
}
