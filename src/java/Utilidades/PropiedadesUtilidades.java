/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author estudiante
 */
public class PropiedadesUtilidades {
     public static Properties loadProperty(String propertiesURL){
         try {
             Properties properties = new Properties();
             System.out.println(propertiesURL);
             InputStream inputStream = PropiedadesUtilidades.class.getClassLoader().getResourceAsStream(propertiesURL);
             properties.load(inputStream);
             return properties;
         } catch (Exception e) {
             e.printStackTrace();
             return null;
         }
     }
}
