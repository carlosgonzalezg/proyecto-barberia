<%-- 
    Document   : adminServicios
    Created on : 05-jun-2019, 14:07:33
    Author     : carlos
--%>

<%@page import="DATABASE.MysqlDbAdapter"%>
<%@page import="java.util.List"%>
<%@page import="Model.ServicioDTO"%>
<%@page import="Model.ServicioDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Admin Servicios</title>
    </head>
    <body>
        <nav class="nav nav-pills nav-justified">
            <a class="nav-item nav-link" href="adminProductos.jsp">Productos</a>
            <a class="nav-item nav-link" href="adminGaleria.jsp">Galeria</a>
            <a class="nav-item nav-link active" href="adminServicios.jsp">Servicios</a>
            <a class="nav-item nav-link" href="vercitas.jsp">Ver citas</a>
            <a class="nav-item nav-link" href="verclientes.jsp">Ver clientes</a>
            <a class="nav-item nav-link" href="vercontactenos.jsp">Ver contactenos</a>
            <a class="nav-item nav-link" href="../../Principal/index.jsp">Home</a> 
        </nav>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre Servicio</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Descripcion</th>  
                       <th class="text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <%
                    MysqlDbAdapter ma = new MysqlDbAdapter();
                    ma.getConnection();
                    ServicioDAO servicio = new ServicioDAO();
                    List<Object> servicios = servicio.mostrarContenido();
                    for (Object s : servicios) {%>
                    <% ServicioDTO serv = (ServicioDTO)s; %>
                <tr>
                    <th scope="row"><%=serv.getNombre()%></th>
                    <td><%="$"+serv.getPrecio()%></td>
                    <td><%=serv.getDescripcion()%></td>
                     <td class="text-center">
                    <a class="btn btn-primary" href="../Admin/Controladores/EliminarServicio.jsp?id=<%=serv.getID()%>" role="button">Eliminar</a>
                     <a href="Controladores/modificarServicios.jsp?id=<%= serv.getID() %>" class="btn btn-primary">Editar</a>
                     </td>

                </tr>
                <% }%>
            </tbody>
        </table>

        <form class="form-inline" action="../Admin/Controladores/AgregarServicio.jsp" method="GET">
            <label class="sr-only" for="nombreProducto">Nombre Servicio</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="nombreServicio" name="nombreServicio" placeholder="nombre Del Servicio">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Descripcion Servicio</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">$</div>
                </div>
                <input type="number" class="form-control" id="precioServicio" name="precioServicio" placeholder="Precio Servicio">
            </div>
            <label class="sr-only" for="urlImagen">Url Imagen</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="descripcionServicio" name="descripcionServicio" placeholder="descripcion Servicio">
            
            
            <button type="submit" class="btn btn-primary mb-2">Guardar</button>

        </form>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>