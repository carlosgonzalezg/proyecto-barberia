<%-- 
    Document   : vercontactenos
    Created on : 06-jun-2019, 19:50:57
    Author     : carlos
--%>

<%@page import="DATABASE.MysqlDbAdapter"%>
<%@page import="java.util.List"%>
<%@page import="Model.ContactenosDTO"%>
<%@page import="Model.ContactenosDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Admin Contactenos</title>
    </head>
    <body>
        <nav class="nav nav-pills nav-justified">
            <a class="nav-item nav-link" href="adminProductos.jsp">Productos</a>
            <a class="nav-item nav-link" href="adminGaleria.jsp">Galeria</a>
            <a class="nav-item nav-link" href="adminServicios.jsp">Servicios</a>
            <a class="nav-item nav-link" href="vercitas.jsp">Ver citas</a>
            <a class="nav-item nav-link" href="verclientes.jsp">Ver clientes</a>
            <a class="nav-item nav-link active" href="vercontactenos.jsp">Ver contactenos</a>
            <a class="nav-item nav-link" href="../../Principal/index.jsp">Home</a> 
        </nav>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Asunto</th>  
                    <th scope="col">Mensaje</th
                </tr>
            </thead>
            <tbody>
                <%
                    MysqlDbAdapter ma = new MysqlDbAdapter();
                    ma.getConnection();
                    ContactenosDAO contacto = new ContactenosDAO();
                    List<Object> contactenos = contacto.mostrarContenido();
                    for (Object c : contactenos) {%>
                <% ContactenosDTO cc = (ContactenosDTO) c;%>
                <tr>
                    <th scope="row"><%=cc.getNombre()%></th>
                    <td><%=cc.getCorreo()%></td>
                    <td><%=cc.getAsunto()%></td>
                    <td><%=cc.getMensaje()%></td>
                </tr>
                <% }%>
            </tbody>
        </table>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
