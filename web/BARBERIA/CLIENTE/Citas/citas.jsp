<%-- 
    Document   : citas
    Created on : 5/06/2019, 09:47:04 PM
    Author     : usuario
--%>
<%@page import="Model.ServicioDTO"%>
<%@page import="Model.ServicioDAO"%>
<%@page import="java.util.List"%>
<%@page import="DATABASE.MysqlDbAdapter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen"
              href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Citas</title>
    </head>
    <body>



        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <div class="card text-center" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Agendar Cita</h5>
                            <p class="card-text">Para agendar cita Primero debes estar registrado</p>
                            <a href="../Registro/registro.jsp" class="btn btn-primary">Registrarme</a>
                        </div>
                    </div>
                        <form class="form-horizontal" action="controlcitas.jsp" method="GET">
                            <fieldset>


                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="cedula" name="cedula" type="number" placeholder="Cedula" class="form-control">
                                    </div>
                                </div>



                                <div id="datetimepicker4" class="input-append">
                                    <input data-format="dd-MM-yyyy" name="fecha" type="text"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                    </span>
                                </div>


                                <div id="datetimepicker3" class="input-append">
                                    <input data-format="hh:mm:ss" name="hora" type="text"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                    </span>
                                </div>




                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="telefono" name="password" type="password" placeholder="Contraseña" class="form-control">
                                    </div>
                                </div>


                                <select name="servicio" class="browser-default custom-select">
                                    <%
                                        MysqlDbAdapter ma = new MysqlDbAdapter();
                                        ma.getConnection();
                                        ServicioDAO servicio = new ServicioDAO();
                                        List<Object> servicios = servicio.mostrarContenido();
                                        for (Object s : servicios) {%>
                                    <% ServicioDTO serv = (ServicioDTO) s;%>
                                    <option value="<%=serv.getID()%>"><%=serv.getNombre()%></option>
                                    <% }%>
                                </select>

                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-lg">Agendar</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"
                src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
        </script> 
        <script type="text/javascript"
                src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
        </script>
        <script type="text/javascript"
                src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
        </script>
        <script type="text/javascript"
                src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker({
                    pickTime: false
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    pickDate: false
                });
            });
        </script>
    </body>
</html>