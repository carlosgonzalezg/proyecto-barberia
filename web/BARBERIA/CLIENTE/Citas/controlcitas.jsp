<%-- 
    Document   : CrearCita
    Created on : 5/06/2019, 10:09:21 PM
    Author     : usuario
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="DATABASE.MysqlDbAdapter"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Model.CitasDAO"%>
<%@page import="Model.CitasDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    MysqlDbAdapter ms = new MysqlDbAdapter();
    int cedula = Integer.parseInt(request.getParameter("cedula"));
    String contrasena = request.getParameter("password");
    System.out.println(contrasena);
    System.out.println(cedula);
    Connection connection = ms.getConnection();
    try {
        PreparedStatement statement1 = connection.prepareStatement("SELECT * FROM cliente WHERE cedula = ?");
        statement1.setInt(1, cedula);
        ResultSet results = statement1.executeQuery();
        while (results.next()) {
            if (contrasena.equals(results.getString(6))) {
                String fecha = request.getParameter("fecha");
                String hora = request.getParameter("hora");
                int servicio = Integer.parseInt(request.getParameter("servicio"));
                CitasDTO cita = new CitasDTO(cedula, (fecha + hora), servicio);
                CitasDAO c = new CitasDAO();
                c.crear(cita);
                response.sendRedirect("../../../Principal/index.jsp");
            } else {
                response.sendRedirect("citas.jsp");
            }
        }

    } catch (Exception e) {
        response.sendRedirect("citas.jsp");
    }

%>