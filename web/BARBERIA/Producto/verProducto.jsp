<%-- 
    Document   : verproducto
    Created on : 7/05/2019, 10:56:37 AM
    Author     : Barberia
--%>

<%@page import="DATABASE.MysqlDbAdapter"%>
<%@page import="java.util.List"%>
<%@page import="Model.ProductoDTO"%>
<%@page import="Model.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>  
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <%
                    MysqlDbAdapter ma = new MysqlDbAdapter();
                    ma.getConnection();
                    ProductoDAO producto = new ProductoDAO();
                    List<ProductoDTO> productos = producto.findAllProducts();
                    for (ProductoDTO pro : productos) {%>
                <tr>
                    <th scope="row"><%=pro.getIdProducto()%></th>
                    <td><%=pro.getNombreProducto()%></td>
                    <td><%="$ " + pro.getPrecioProducto()%></td>
                    <td> <a class="btn btn-primary" href="EliminarProducto.jsp?id=<%=pro.getIdProducto()%>" role="button">eliminar</a></td>

                </tr>
                <% }%>
            </tbody>
        </table>

        <form class="form-inline" action="CreateProduct.jsp" method="GET">
            <label class="sr-only" for="nombreProducto">id</label>
            <input type="number" class="form-control mb-2 mr-sm-2" id="id" name="id" placeholder="IDD">
            <label class="sr-only" for="nombreProducto">Nombre Producto</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="nombreProducto" name="nombreProducto" placeholder="Nombre Del Producto">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Precio producto</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">$</div>
                </div>
                <input type="number" class="form-control" id="precioProducto" name="precioProducto" placeholder="Precio Producto">
            </div>

            <button type="submit" class="btn btn-primary mb-2">Guardar</button>

        </form>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>