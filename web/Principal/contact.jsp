<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Contactenos</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

        <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">

        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

        <link rel="stylesheet" href="css/aos.css">

        <link rel="stylesheet" href="css/ionicons.min.css">

        <link rel="stylesheet" href="css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="css/jquery.timepicker.css">


        <link rel="stylesheet" href="css/flaticon.css">
        <link rel="stylesheet" href="css/icomoon.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="index.html">El Artista Barbershop</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>

                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="index.jsp" class="nav-link">Inicio</a></li>
                    <li class="nav-item"><a href="services.jsp" class="nav-link">Servicios &amp; Productos</a></li>
                    <li class="nav-item"><a href="gallery.jsp" class="nav-link">Galeria</a></li>
                    <li class="nav-item active"><a href="contact.jsp" class="nav-link">Contactenos</a></li>
                    <li class="nav-item"><a href="../BARBERIA/CLIENTE/Inicio/iniciar.jsp" class="nav-link">Iniciar Sesion</a></li>
                    <li class="nav-item"><a href="../BARBERIA/CLIENTE/Registro/registro.jsp" class="nav-link">Registrarse</a></li>
                </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->

        <div class="hero-wrap" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
                    <div class="col-md-6 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                        <h1 class="mb-3 mt-5 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Contactenos</h1>
                        <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="index.html">Inicio</a></span> <span>Contactenos</span></p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-section contact-section">
            <div class="container mt-5">
                <div class="row block-9">
                    <div class="col-md-4 contact-info ftco-animate">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <h2 class="h4">Informacion de Contacto</h2>
                            </div>
                            <div class="col-md-12 mb-3">
                                <p><span>Direccion:</span> Calle 2 Avenida los Faroles #11E 99 A una cuadra de la UFPS</p>
                            </div>
                            <div class="col-md-12 mb-3">
                                <p><span>Celular:</span> <a href="Celular">310 8124888</a></p>
                            </div>
                            <div class="col-md-12 mb-3">
                                <p><span>Email:</span> <a href="mail">elartistabarbershop@gmail.com</a></p>
                            </div>
                            <div class="col-md-12 mb-3">
                                <p><span>Sitio Web:</span> <a href="index.jsp">www.elartistabarbershop.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6 ftco-animate">
                        <form action="controlcontacto.jsp" class="contact-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="nombre" class="form-control" placeholder="Tu Nombre">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" placeholder="Tu E-Mail">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="asunto" class="form-control" placeholder="Asunto">
                            </div>
                            <div class="form-group">
                                <textarea name="mensaje" id="mensaje" cols="30" rows="7" class="form-control" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Enviar Mensaje" class="btn btn-primary py-3 px-5">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <div id="map"></div>

        <footer class="ftco-footer ftco-section img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Sobre nosotros: </h2>
                        <p>Bienvenidos a la barber�a Un santuario masculino para desconectarse del mundo, en donde entendemos las inquietudes del hombre de hoy para ofrecer una imagen actual y el estilo que mejor refleja su personalidad, un corte impecable y una tradicional afeitada, todo al rededor de un bar, un ambiente exclusivo para hombres.</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li class="ftco-animate"><a href="https://www.facebook.com/El-Artista-Barber-Shop-2160319640730712/"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="https://www.instagram.com/elartistabarbershop/"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">Servicios</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">Corte caballero y ni�os</a></li>
                            <li><a href="#" class="py-2 d-block">Corte Barba</a></li>
                            <li><a href="#" class="py-2 d-block">Limpiezas</a></li>
                            <li><a href="#" class="py-2 d-block">Pigmentaci�n de cejas y Barba</a></li>
                            <li><a href="#" class="py-2 d-block">Depilaci�n en cera</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Contacto</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Calle 2 Avenida los Faroles #11E 99</span></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">310 8124888</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">elartistabarbershop@gmail.com</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos Los Derechos Reservados | Made by ING SISTEMAS UFPS
                     </p>
                </div>
            </div>
        </div>
    </footer>


        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/aos.js"></script>
        <script src="js/jquery.animateNumber.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.timepicker.min.js"></script>
        <script src="js/scrollax.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
        <script src="js/google-map.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>