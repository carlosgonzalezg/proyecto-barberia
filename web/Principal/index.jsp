<%@page import="Model.ImagenDTO"%>
<%@page import="Model.ImagenDAO"%>
<%@page import="Model.ServicioDTO"%>
<%@page import="Model.ServicioDAO"%>
<%@page import="Model.ProductoDTO"%>
<%@page import="java.util.List"%>
<%@page import="Model.ProductoDAO"%>
<%@page import="DATABASE.MysqlDbAdapter"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>El artista barbershop</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

        <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">

        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">

        <link rel="stylesheet" href="css/aos.css">

        <link rel="stylesheet" href="css/ionicons.min.css">

        <link rel="stylesheet" href="css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="css/jquery.timepicker.css">


        <link rel="stylesheet" href="css/flaticon.css">
        <link rel="stylesheet" href="css/icomoon.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="index.jsp">El Artista Barbershop</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>
               <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a href="index.jsp" class="nav-link">Inicio</a></li>
                    <li class="nav-item"><a href="services.jsp" class="nav-link">Servicios &amp; Productos</a></li>
                    <li class="nav-item"><a href="gallery.jsp" class="nav-link">Galeria</a></li>
                    <li class="nav-item"><a href="contact.jsp" class="nav-link">Contactenos</a></li>
                    <li class="nav-item"><a href="../BARBERIA/CLIENTE/Inicio/iniciar.jsp" class="nav-link">Iniciar Sesion</a></li>
                    <li class="nav-item"><a href="../BARBERIA/CLIENTE/Registro/registro.jsp" class="nav-link">Registrarse</a></li>
                </ul>
              </div>
            </div>
        </nav>
        <!-- END nav -->
        <div class="hero-wrap js-fullheight" style="background-image: url('images/bg_2.jpeg')

             ;" >
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
                    
                    <a href="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F2160319640730712%2Fvideos%2F753708695048549%2F&show_text=1&width=560" class="icon popup-vimeo d-flex justify-content-center align-items-center">
                        <span class="icon-play"></a>
                    </a>
                    <div class="col-md-6 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                        <p class="mb-3" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Un espacio exclusivo para tu comodidad</p>
                        <h1 class="mb-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">El Artista Barbershop</h1>
                        <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><a href="../BARBERIA/CLIENTE/Citas/citas.jsp" class="btn btn-primary px-4 py-3">Reservar Una Cita</a></p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-intro">
            <div class="container-wrap">
                <div class="wrap d-md-flex align-items-end">
                    <div class="info">
                        <div class="row no-gutters">
                            <div class="col-md-4 d-flex ftco-animate">
                                <div class="icon"><span class="icon-phone"></span></div>
                                <div class="text">
                                    <h3>310 8124888</h3>
                                    <p>Telefono</p>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex ftco-animate">
                                <div class="icon"><span class="icon-my_location"></span></div>
                                <div class="text">
                                    <h3>Calle 2 Avenida los Faroles #11E 99</h3>
                                    <p>A una cuadra de la UFPS</p>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex ftco-animate">
                                <div class="icon"><span class="icon-clock-o"></span></div>
                                <div class="text">
                                    <h3>Abierto de</h3>
                                    <h3>Lunes-Sabado</h3>
                                    <p>8:00am - 9:00pm</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="social pl-md-5 p-4">
                        <ul class="social-icon">

                            <li class="ftco-animate"><a href="https://www.facebook.com/El-Artista-Barber-Shop-2160319640730712/"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="https://www.instagram.com/elartistabarbershop/"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="ftco-section ">
            <div class="container">
                <div class="row justify-content-center mb-4">
                    <div class="col-md-7 heading-section ftco-animate text-center">
                        <h2 class="mb-4">Bienvenidos a <span>El Artista BarberShop</span> </h2>
                        <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center ftco-animate">
                        <p>Bienvenidos a la barber�a
                            Un santuario masculino para desconectarse del mundo, en donde entendemos las inquietudes del hombre de hoy para ofrecer una imagen actual y el estilo que mejor refleja su personalidad, un corte impecable y una tradicional afeitada, todo al rededor de un bar, un ambiente exclusivo para hombres..</p>
                    </div>
                </div>
            </div>
            <div class="container">
               
                <div class="row justify-content-center mb-5 pb-3 mt-5 pt-5">
                    <div class="col-md-7 heading-section text-center ftco-animate">
                        <h2 class="mb-4">Servicios</h2>
                        <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                        <p class="mt-5"></p>
                    </div>
                </div>
                <div class="row">

                    <%
                        MysqlDbAdapter ma = new MysqlDbAdapter();
                        ma.getConnection();
                        ServicioDAO servicio = new ServicioDAO();
                        List<Object> serv = servicio.mostrarContenido();
                        for (Object s : serv) {%>

                    <%ServicioDTO ser = (ServicioDTO) s;%>
                    <div class="col-md-6">
                        <div class="pricing-entry ftco-animate">
                            <div class="d-flex text align-items-center">
                                <h3><span><%=ser.getNombre()%></span></h3>
                                <span class="price">$ <%=ser.getPrecioFormato()%></span>
                            </div>
                            <div class="d-block">
                                <p><%=ser.getDescripcion()%></p>
                            </div>
                        </div>
                    </div>

                    <% }%>
                </div>
            </div>
        </section>


        <section class="ftco-section ftco-discount img" style="background-image: url(images/bg_4.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center" data-scrollax-parent="true">
                    <div class="col-md-7 text-center discount ftco-animate" data-scrollax=" properties: { translateY: '-30%'}">
                        <h3>25% de Descuento en tu primera cita</h3>
                        <h2 class="mb-4">En todos los Servicios</h2>
                        <p class="mb-4">Obten el 25% de descuento en todos nuestros servicios al reservar tu primera cita, ademas el 50% el dia de tus cumplea�os.</p>
                        <p><a href="../BARBERIA/CLIENTE/Inicio/iniciar.jsp" class="btn btn-primary px-4 py-3">Iniciar Sesion</a></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="ftco-section ftco-bg-dark">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
                    <div class="col-md-7 heading-section ftco-animate text-center">
                        <h2 class="mb-4">Nuestra Tienda</h2>
                        <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                        <p class="mt-5">Nuestros ultimos productos.</p>
                    </div>
                </div>
                <div class="row">

                    <%
                        MysqlDbAdapter mas = new MysqlDbAdapter();
                        mas.getConnection();
                        ProductoDAO producto = new ProductoDAO();
                        List<ProductoDTO> productos = producto.findAllProducts();
                        int id=1;
                        int c=productos.size();
                            for (ProductoDTO pro : productos) {
                             if(c-id<4){%>
                    <div class="col-md-3 ftco-animate">
                        <div class="product-entry text-center">
                            <a href="#"><img src="<%=pro.getUrlImagen()%>" class="img-fluid" alt="Colorlib Template"></a>
                            <div class="text">
                                <h3><a href="#"><%=pro.getNombreProducto()%></a></h3>
                                <span class="price mb-4">$ <%=pro.getPrecioFormato()%></span>
                                <p><a href="#" class="btn btn-primary">Comprar</a></p>
                            </div>
                        </div>  
                    </div>
                    <%} id++;}%>
                </div>      

            </div>
        </div>
    </section>

    <section class="ftco-gallery">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Nuestra Galeria</h2>
                    <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                    <p class="mt-5">Ultimas fotos</p>
                </div>
            </div>
        </div>
            <div class="container-wrap">
                <div class="row no-gutters">
                    <%
                        MysqlDbAdapter m = new MysqlDbAdapter();
                        m.getConnection();
                        ImagenDAO img = new ImagenDAO();
                        List<ImagenDTO> imagenes = img.mostrarContenido();
                        int cant=imagenes.size();
                        int ini=1;
                        for (ImagenDTO im : imagenes) {
                        if(cant-ini<4){%>
                    <div class="col-md-3 ftco-animate">
                        <a href="<%=im.getUrl()%>" class="gallery img image-popup d-flex align-items-center" style="background-image: url(<%=im.getUrl()%>);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                                <span class="icon-search"></span>
                            </div>
                        </a>
                    </div>
                            <%} ini++;}%>
                </div>
            </div>
        
    </section>

   
    <footer class="ftco-footer ftco-section img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Sobre nosotros: </h2>
                        <p>Bienvenidos a la barber�a Un santuario masculino para desconectarse del mundo, en donde entendemos las inquietudes del hombre de hoy para ofrecer una imagen actual y el estilo que mejor refleja su personalidad, un corte impecable y una tradicional afeitada, todo al rededor de un bar, un ambiente exclusivo para hombres.</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li class="ftco-animate"><a href="https://www.facebook.com/El-Artista-Barber-Shop-2160319640730712/"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="https://www.instagram.com/elartistabarbershop/"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">Servicios</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">Corte caballero y ni�os</a></li>
                            <li><a href="#" class="py-2 d-block">Corte Barba</a></li>
                            <li><a href="#" class="py-2 d-block">Limpiezas</a></li>
                            <li><a href="#" class="py-2 d-block">Pigmentaci�n de cejas y Barba</a></li>
                            <li><a href="#" class="py-2 d-block">Depilaci�n en cera</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Contacto</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Calle 2 Avenida los Faroles #11E 99</span></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">310 8124888</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">elartistabarbershop@gmail.com</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos Los Derechos Reservados | Made by ING SISTEMAS UFPS
                     </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/jquery.timepicker.min.js"></script>
    <script src="js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script>
    <script src="js/main.js"></script>

</body>
</html>